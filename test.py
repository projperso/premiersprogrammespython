from passlib.hash import sha256_crypt

# Demande à l'utilisateur de saisir un mot de passe
password = input("Entrez votre mot de passe : ")

# Hache le mot de passe
hashed_password = sha256_crypt.hash(password)

# Affiche le mot de passe haché
print("Mot de passe haché :", hashed_password)

# Demande à l'utilisateur de saisir le mot de passe à vérifier
password_to_check = input("Entrez le mot de passe à vérifier : ")

# Vérifie si le mot de passe correspond au hachage
if sha256_crypt.verify(password_to_check, hashed_password):
    print("Mot de passe correct.")
else:
    print("Mot de passe incorrect.")

