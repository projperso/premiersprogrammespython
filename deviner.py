from random import choice

def jouer():
	print("pensez à un nombre en 0 et 100")
	borne_min = 0
	borne_max = 100
	
	trouve = False
	while not trouve:
		milieu = (borne_min + borne_max) //2
		print("je pense a", milieu)
		print("que pensez-vous de mon choix")
		print("1. Gagné !")
		print("2. Trop petit")
		print("3. Trop grand")
		reponse = int (input())
		if reponse == 1:
			print("je suis trop fort")
			trouve = True
		elif reponse == 3:
			borne_max = milieu
		else:
			if borne_max - borne_min > 1:
				borne_min = milieu
			else:
				borne_min = borne_min + 1
if __name__ == '__main__':
	jouer()
			
